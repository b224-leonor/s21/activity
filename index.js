//console.log("Hello")

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

let wrestler = ['The Miz', 'Stone Cold', 'Triple H', 'HBK', 'Undertaker']
console.log(wrestler)

function wwe(wrestlerName) {
  wrestler[wrestler.length] = wrestlerName
}

wwe('John Cena')
console.log(wrestler)

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function getWrestlerByIndex(index) {
  return wrestler[index]
}

let itemFound = getWrestlerByIndex(2)
console.log(itemFound)

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/
function deleteLastWrestler() {
  let lastWrestler = wrestler[wrestler.length - 1]
  wrestler.length = wrestler.length - 1
  return lastWrestler
}

let deletedWrestler = deleteLastWrestler()
console.log(deletedWrestler)

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/
function updateWrestler(update, index) {
  wrestler[index] = update
}

updateWrestler('The Rock', 2)
console.log(wrestler)

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/
function deleteAllWrestlers() {
  wrestler.length = 0
}

deleteAllWrestlers()
console.log(wrestler)

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function isWrestlerEmpty() {
  if (wrestler.length > 0) {
    return false
  } else {
    return true
  }
}

let isWrestlerArrayEmpty = isWrestlerEmpty()

console.log(isWrestlerArrayEmpty)
